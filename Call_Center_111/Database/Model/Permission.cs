﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{
    public class Permission : IEntityTypeConfiguration<Permission>
    {
        public int PermissionId { get; set; }
        public string PermissionName { get; set; }
        public bool IsDelete { get; set; }

        public void Configure(EntityTypeBuilder<Permission> builder)
        {
            builder.ToTable("Permissions").HasKey(x => x.PermissionId);
            builder.Property(x => x.PermissionId).HasColumnName("PermissionId").IsRequired();
            builder.Property(x => x.PermissionName).HasColumnName("PermissionName").HasMaxLength(200);
            builder.Property(x => x.IsDelete).HasDefaultValue(false);
        }
    }
}
