﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{
    public class Advise : IEntityTypeConfiguration<Advise>
    {
        public int AdviseId { get; set; }
        public string ListenLink { get; set; }
        public string AdviseContents { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsDeleted { get; set; }

        public int ProfileId { get; set; }
        public Profile Profile { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public void Configure(EntityTypeBuilder<Advise> builder)
        {
            builder.ToTable("Advises").HasKey(x => x.AdviseId);
            builder.Property(x => x.AdviseId).IsRequired();
            builder.Property(x => x.ListenLink);
            builder.Property(x => x.AdviseContents).HasMaxLength(2000);
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);

            builder.HasOne(x => x.Profile).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_Profile_Advise").HasForeignKey("ProfileId");
            builder.HasOne(x => x.User).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_User_Advise").HasForeignKey("UserId");
        }
    }
}
