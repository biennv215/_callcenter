﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{ 
    public class District : IEntityTypeConfiguration<District>
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set;}
        public int ProvinceId { get; set; }
        public Province Province { get; set; }

        public void Configure(EntityTypeBuilder<District> builder)
        {
            builder.HasOne(x => x.Province).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_Province_District").HasForeignKey("ProvinceId");
        }
    }
}
