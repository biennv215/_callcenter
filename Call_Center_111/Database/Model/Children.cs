﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{
    public class Children : IEntityTypeConfiguration<Children>
    {
        public int ChildrenId { get; set; }
        public string ChildrenName { get; set; }
        public int Age { get; set; }
        public bool Gender { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsDeleted { get; set; }
        public void Configure(EntityTypeBuilder<Children> builder)
        {
            builder.ToTable("Childrens").HasKey(x => x.ChildrenId);
            builder.Property(x => x.ChildrenId).IsRequired();
            builder.Property(x => x.ChildrenName).HasMaxLength(50);
            builder.Property(x => x.Age);
            builder.Property(x => x.Gender).HasDefaultValue(false);
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);
        }
    }
}
