﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Database.Model
{
    public class Profile : IEntityTypeConfiguration<Profile>
    {
        public int ProfileId { get; set; }
        public string PhoneNumber   { get; set; }
        public string ProblemContents { get; set; }
        public string Status { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsDeleted { get; set; }

        public int ChildrenId { get; set; }
        public Children Children { get; set; }
        public int ProvinceId { get; set; }
        public Province province { get; set; }
        public int DistrictId { get; set; }
        public District district { get; set; }
        public int WardsId { get; set; }
        public Wards wards { get; set; }
        public void Configure(EntityTypeBuilder<Profile> builder)
        {
            builder.ToTable("Profiles").HasKey(x => x.ProfileId);
            builder.Property(x => x.ProfileId).IsRequired();
            builder.Property(x => x.PhoneNumber).HasMaxLength(10);
            builder.Property(x => x.ProblemContents).HasMaxLength(2000);
            builder.Property(x => x.Status).HasMaxLength(50);
            builder.Property(x => x.IsDeleted).HasDefaultValue(false);

            builder.HasOne(x => x.Children).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_Children_Profile").HasForeignKey("ChildrenId");
            builder.HasOne(x => x.province).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_Province_Profile").HasForeignKey("ProvinceId");
            builder.HasOne(x => x.district).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_District_Profile").HasForeignKey("DistrictId");
            builder.HasOne(x => x.wards).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_Wards_Profile").HasForeignKey("WardsId");
        }
    }
}
