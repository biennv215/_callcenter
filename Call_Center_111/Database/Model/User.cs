﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Database.Model
{
    public class User : IEntityTypeConfiguration<User>
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsDeleted { get; set; }
        public int PermissionId { get; set; }
        public Permission permission { get; set; }

        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users").HasKey(x => x.UserId);
            builder.Property(x => x.UserId).IsRequired();
            builder.Property(x => x.UserName).HasMaxLength(50);
            builder.Property(x => x.Account).HasMaxLength(50);
            builder.Property(x => x.Password).HasMaxLength(50);
            builder.Property(x=>x.IsDeleted).HasDefaultValue(false);

            builder.HasOne(x => x.permission).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_User_Permission").HasForeignKey("PermissionId");
        }
    }
}
