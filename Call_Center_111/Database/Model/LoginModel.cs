﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{
    public class LoginModel
    {
        public string UserName { set; get; }
        public string Password { set; get; }
        public bool Remember { set; get; }
    }
}
