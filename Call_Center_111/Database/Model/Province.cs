﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{
    public class Province : IEntityTypeConfiguration<Province>
    {
        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }

        public void Configure(EntityTypeBuilder<Province> builder)
        {
            builder.ToTable("Provinces").HasKey(x => x.ProvinceId);
            builder.Property(x => x.ProvinceName).HasMaxLength(50);
        }
    }
}
