﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{
    public class Wards : IEntityTypeConfiguration<Wards>
    {
        public int WardsId { get; set; }
        public string WardsName { get; set;}
        public int DistrictId { get; set; }
        public District district { get; set; }

        public void Configure(EntityTypeBuilder<Wards> builder)
        {
            builder.HasOne(x => x.district).WithMany().OnDelete(DeleteBehavior.NoAction)
            .HasConstraintName("PK_Wards_District").HasForeignKey("DistrictId");
        }
    }
}
