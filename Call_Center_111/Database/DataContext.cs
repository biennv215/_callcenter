﻿using System;
using System.Collections.Generic;
using System.Text;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Wards> Wards { get; set; }
        public DbSet<Advise> Relationships { get; set; }
        public DbSet<Children> Childrens { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new Permission());
            modelBuilder.ApplyConfiguration(new User());
            modelBuilder.ApplyConfiguration(new Province());
            modelBuilder.ApplyConfiguration(new District());
            modelBuilder.ApplyConfiguration(new Wards());
            modelBuilder.ApplyConfiguration(new Children());
            modelBuilder.ApplyConfiguration(new Profile());
            modelBuilder.ApplyConfiguration(new Advise());
        }
    }
}
