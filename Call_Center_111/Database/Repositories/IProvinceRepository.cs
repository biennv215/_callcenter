﻿using Database.Infrastructures;
using Database.Model;

namespace Database.Repositories
{
    public interface IProvinceRepository: IBaseRepository<Province>
    {
    }
}