﻿using Database.Infrastructures;
using Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.Repositories
{
    public class MyProfileRepository : BaseRepository<Profile>, IProfileRepository
    {
        private readonly DataContext DataContext = null;
        public MyProfileRepository(DataContext dataContext): base(dataContext)
        {
            DataContext = dataContext;
        }
        public List<Profile> GetProfile()
        {
            return DataContext.Profiles.Where(x => x.IsDeleted == false).ToList();
        }
        public Profile ProfileGetById(int profileid)
        {
            var profile = DataContext.Profiles.SingleOrDefault(x => x.ProfileId == profileid);
            return profile;
        }
        public List<Profile> GetProfileSearch(string phonenumber)
        {
            var result = DataContext.Profiles.Where(x => x.PhoneNumber.Contains(phonenumber)).ToList();
            return result;
        }
        public bool InsertProfile(int profileid, string phonenumber, string profilecontent, string profilestatus, int createby)
        {
            Profile profile = new Profile();
            profile.ProfileId = profileid;
            profile.PhoneNumber = phonenumber;
            profile.CreateBy = createby;
            profile.Status = profilestatus;
            profile.CreateTime = DateTime.Now;
            DataContext.Profiles.Add(profile);
            DataContext.SaveChanges();
            return true;
        }
        public bool UpdateProfile(int profileid, string phonenumber, string profilecontent, string profilestatus, int updateby)
        {
            try
            {
                Profile result = DataContext.Profiles.Where(x => x.ProfileId == profileid).FirstOrDefault();
                if (result == null)
                    return false;
                else
                {
                    result.PhoneNumber = phonenumber;
                    result.ProblemContents = profilecontent;
                    result.Status = profilestatus;
                    result.UpdateBy = updateby;
                    result.UpdateTime = DateTime.Now;
                    DataContext.Profiles.Update(result);
                    DataContext.SaveChanges();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteProfile(int profileid)
        {
            var result = DataContext.Profiles.Where(x => x.ProfileId == profileid).SingleOrDefault();
            if (result == null)
                return false;
            else
            {
                result.IsDeleted = true;
                DataContext.SaveChanges();
                return true;
            }
        }
    }
}
