﻿using Database.Infrastructures;
using Database.Model;

namespace Database.Repositories
{
    public class MyWardsRepository: BaseRepository<Wards>, IWardsRepository
    {
        public MyWardsRepository(DataContext context): base(context)
        {
        }
    }
}