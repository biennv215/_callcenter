﻿using Database.Infrastructures;
using Database.Model;
using System;
using System.Collections.Generic;

namespace Database.Repositories
{
    public interface IChildrenRepository: IBaseRepository<Children>
    {
    }
}