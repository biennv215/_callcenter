﻿using Database.Infrastructures;
using Database.Model;

namespace Database.Repositories
{
    public interface IWardsRepository: IBaseRepository<Wards>
    {
    }
}