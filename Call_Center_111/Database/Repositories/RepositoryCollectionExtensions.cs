﻿using Database.Repository;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Repositories
{
    public static class RepositoryCollectionExtensions
    {
        public static void UseMyRepository(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, MyUserRepository>();
            services.AddScoped<IProfileRepository, MyProfileRepository>();
            services.AddScoped<IChildrenRepository, MyChildrenRepository>();
            services.AddScoped<IProvinceRepository, MyProvinceRepository>();
            services.AddScoped<IDistrictRepository, MyDistrictRepository>();
            services.AddScoped<IWardsRepository, MyWardsRepository>();
        }
    }
}
