﻿using Database.Infrastructures;
using Database.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Repositories
{
    public interface IProfileRepository : IBaseRepository<Profile>
    {
        List<Profile> GetProfile();
        public Profile ProfileGetById(int profileid);
        List<Profile> GetProfileSearch(string phonenumber);
        bool InsertProfile(int profileid, string phonenumber, string profilecontent, string profilestatus, int createby);
        bool UpdateProfile(int profileid, string phonenumber, string profilecontent, string profilestatus, int updateby);
        bool DeleteProfile(int profileid);
    }
}
