﻿using Database.Infrastructures;
using Database.Model;

namespace Database.Repositories
{
    public class MyDistrictRepository: BaseRepository<District>, IDistrictRepository
    {
        public MyDistrictRepository(DataContext context): base(context)
        {

        }
    }
}