﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.Model;

namespace Database.Repository
{
    public class MyUserRepository : IUserRepository
    {
        private readonly DataContext DataContext = null;
        public MyUserRepository(DataContext dataContext)
        {
            DataContext = dataContext;
        }

        public User GetUser(string username, string password)
        {
            var result = DataContext.Users.Where(x=>x.Account==username && x.Password == password && x.IsDeleted == false).FirstOrDefault();
            return result;
            //throw new NotImplementedException();
        }

        public User ChangePassword(string username, string new_password)
        {
            User user = DataContext.Users.Where(x=>x.Account == username).FirstOrDefault();
            user.Account = username;
            user.Password = new_password;
            DataContext.Users.Update(user);
            DataContext.SaveChanges();
            return user;
        }
    }
}
