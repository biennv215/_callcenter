﻿using Database.Infrastructures;
using Database.Model;

namespace Database.Repositories
{
    public class MyProvinceRepository: BaseRepository<Province>, IProvinceRepository
    {
        public MyProvinceRepository(DataContext context): base(context)
        {

        }
    }
}