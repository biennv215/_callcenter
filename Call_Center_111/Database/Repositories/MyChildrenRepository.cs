﻿using Database.Infrastructures;
using Database.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Database.Repositories
{
    public class MyChildrenRepository : BaseRepository<Children>, IChildrenRepository
    {
        public MyChildrenRepository( DataContext context): base(context)
        {

        }
    }
}