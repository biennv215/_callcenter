﻿using Database.Repositories;

namespace Database.Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private DataContext Context { get; }
        private IChildrenRepository childrenRepository;
        private IProfileRepository profileRepository;
        private IProvinceRepository provinceRepository;
        private IDistrictRepository districtRepository;
        private IWardsRepository wardsRepository;

        public UnitOfWork(DataContext context)
        {
            this.Context = context;
        }

        public IChildrenRepository ChildrenRepository => this.childrenRepository ??= new MyChildrenRepository(Context);
        public IProfileRepository ProfileRepository => this.profileRepository ??= new MyProfileRepository(Context);
        public IProvinceRepository ProvinceRepository => this.provinceRepository ??= new MyProvinceRepository(Context);
        public IDistrictRepository DistrictRepository => this.districtRepository ??= new MyDistrictRepository(Context);
        public IWardsRepository WardsRepository => this.wardsRepository ??= new MyWardsRepository(Context);

        public DataContext AppDbContext => this.Context;


        public void Dispose()
        {
            //this.context.Dispose();
        }

        public int SaveChanges()
        {
            return this.Context.SaveChanges();
        }
    }
}