﻿using System;
using System.Collections.Generic;

namespace Database.Infrastructures
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        void Add(TEntity entity);

        void AddRange(List<TEntity> entities);

        void Update(TEntity entity);

        void Delete(int id);

        void Delete(TEntity entity);

        int Count(Func<TEntity, bool> condition);

        int NumberPage(Func<TEntity, bool> search, int limit);

        IEnumerable<TEntity> GetAll();

        TEntity GetById(int id);

        IEnumerable<TEntity> Find(Func<TEntity, bool> condition);

        IEnumerable<TEntity> Pagination(int start, int limit, Func<TEntity, int> condition, Func<TEntity, bool> search);
    }
}