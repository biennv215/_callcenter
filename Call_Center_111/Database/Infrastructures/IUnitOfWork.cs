﻿using Database.Repositories;
using System;

namespace Database.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {
        IChildrenRepository ChildrenRepository { get; }
        IProfileRepository ProfileRepository { get; }
        IProvinceRepository ProvinceRepository { get; }
        IDistrictRepository DistrictRepository { get; }
        IWardsRepository WardsRepository { get; }

        DataContext AppDbContext { get; }

        int SaveChanges();
    }
}