﻿using Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Database.Infrastructures
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected readonly DataContext context;
        protected readonly DbSet<TEntity> dbSet;

        public BaseRepository(DataContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual void Add(TEntity entity)
        {
            this.dbSet.Add(entity);
        }

        public virtual void AddRange(List<TEntity> entities)
        {
            this.dbSet.AddRange(entities);
        }

        public int Count(Func<TEntity, bool> condition)
        {
            return this.dbSet.Where(condition).Count();
        }

        public virtual void Delete(int id)
        {
            var entity = dbSet.Find(id);
            if (entity == null)
                throw new ArgumentException($"{this.dbSet.GetType()} Không tìm thấy id = {id}");
            this.dbSet.Remove(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            this.dbSet.Remove(entity);
        }

        public virtual IEnumerable<TEntity> Find(Func<TEntity, bool> condition)
        {
            return this.dbSet.Where(condition);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return this.dbSet.AsEnumerable();
        }

        public virtual TEntity GetById(int id)
        {
            return this.dbSet.Find(id);
        }

        public int NumberPage(Func<TEntity, bool> search, int limit)
        {
            var entities = dbSet.AsEnumerable();
            var total = entities.Where(search).Count();
            float numberPage = (float)total / limit;
            //var abc = (int)Math.Ceiling(numberPage);
            return (int)Math.Ceiling(numberPage);
        }

        public IEnumerable<TEntity> Pagination(int start, int limit, Func<TEntity, int> condition, Func<TEntity, bool> search)
        {
            var entities = dbSet.AsEnumerable();
            var entitiespage = entities.Where(search).OrderByDescending(condition).Skip(start).Take(limit);
            return entitiespage;
        }

        public virtual void Update(TEntity entity)
        {
            this.dbSet.Update(entity);
        }
    }
}