﻿using System.Collections.Generic;
using ViewModel.Provinces;

namespace Services
{
    public interface IProvinceService
    {
        IEnumerable<ProvinceVM> GetProvinces();
    }
}