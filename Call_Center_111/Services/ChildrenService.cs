﻿using Database.Infrastructures;
using Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using ViewModel.Childrens;

namespace Services
{
    public class ChildrenService : IChildrenService
    {
        private readonly IUnitOfWork unitOfWork = null;

        public ChildrenService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public int ChildrenCount()
        {
            return this.unitOfWork.ChildrenRepository.Count(x=>x.IsDeleted == false);
        }

        public ReponseResult Create(CreateChildrenVM request)
        {
            try
            {
                var children = new Children()
                {
                    ChildrenName = request.ChildrenName,
                    Age = request.Age,
                    Gender = request.Gender,
                    CreateBy = request.CreateBy,
                    CreateTime = DateTime.Now,
                };
                this.unitOfWork.ChildrenRepository.Add(children);
                this.unitOfWork.SaveChanges();
                return new ReponseResult();
            }
            catch (Exception ex)
            {
                return new ReponseResult(ex.Message);
            }
        }

        public ReponseResult Delete(int id)
        {
            try
            {
                var children = this.unitOfWork.ChildrenRepository.GetById(id);
                if (children == null)
                    return new ReponseResult($"Không tìm thấy đối tượng ({id})");
                children.IsDeleted = true;
                this.unitOfWork.SaveChanges();
                return new ReponseResult();
            }
            catch (Exception ex)
            {
                return new ReponseResult(ex.Message);
            }
        }

        public ChildrenVM GetById(int id)
        {
            var children = this.unitOfWork.ChildrenRepository.GetById(id);
            if (children != null && children.IsDeleted == false)
            {
                return new ChildrenVM()
                {
                    ChildrenId = children.ChildrenId,
                    ChildrenName = children.ChildrenName,
                    Age = children.Age,
                    Gender = children.Gender,
                    CreateBy = children.CreateBy,
                    CreateTime = children.CreateTime,
                    UpdateBy = children.UpdateBy,
                    UpdateTime = children.UpdateTime,
                    IsDelete = children.IsDeleted,
                };
            }
            return new ChildrenVM();
        }

        public ChildrenVM GetChildrenNew()
        {
            var children = this.unitOfWork.ChildrenRepository.GetAll().Where(x=>x.IsDeleted==false).LastOrDefault();
            if(children != null)
            {
                return new ChildrenVM()
                {
                    ChildrenId = children.ChildrenId,
                    ChildrenName = children.ChildrenName,
                    Age = children.Age,
                    Gender = children.Gender,
                    CreateBy = children.CreateBy,
                    CreateTime = children.CreateTime,
                    UpdateBy = children.UpdateBy,
                    UpdateTime = children.UpdateTime,
                    IsDelete = children.IsDeleted,
                };
            }
            return new ChildrenVM();
        }

        public IEnumerable<ChildrenVM> GetChildrens()
        {
            var childrens = this.unitOfWork.ChildrenRepository.GetAll();
            foreach (var children in childrens)
            {
                if (children.IsDeleted == false)
                {
                    yield return new ChildrenVM()
                    {
                        ChildrenId = children.ChildrenId,
                        ChildrenName = children.ChildrenName,
                        Age = children.Age,
                        Gender = children.Gender,
                        CreateBy = children.CreateBy,
                        CreateTime = children.CreateTime,
                        UpdateBy = children.UpdateBy,
                        UpdateTime = children.UpdateTime,
                        IsDelete = children.IsDeleted,
                    };
                }
            }
        }

        public int NumberPage(Func<Children, bool> search, int limit)
        {
            return this.unitOfWork.ChildrenRepository.NumberPage(search, limit);
        }

        public IEnumerable<ChildrenVM> PaginationChildrens(int start, int limit, string searchKey = "")
        {
            if (String.IsNullOrEmpty(searchKey))
                searchKey = "";
                
            var childrens = this.unitOfWork.ChildrenRepository.Pagination(start, limit, x => x.ChildrenId, x => x.ChildrenName.Contains(searchKey) && x.IsDeleted == false);
            foreach (var children in childrens)
            {
                yield return new ChildrenVM()
                {
                    ChildrenId = children.ChildrenId,
                    ChildrenName = children.ChildrenName,
                    Age = children.Age,
                    Gender = children.Gender,
                    CreateBy = children.CreateBy,
                    CreateTime = children.CreateTime,
                    UpdateBy = children.UpdateBy,
                    UpdateTime = children.UpdateTime,
                    IsDelete = children.IsDeleted,
                };
            }
        }

        public ReponseResult Update(UpdateChildrenVM request)
        {
            try
            {
                var children = this.unitOfWork.ChildrenRepository.GetById(request.ChildrenId);
                if (children == null)
                    return new ReponseResult($"Không tìm thấy đối tượng ({request.ChildrenId})");
                children.ChildrenName = request.ChildrenName;
                children.Age = request.Age;
                children.Gender = request.Gender;
                children.UpdateBy = 1;
                children.UpdateTime = DateTime.Now;
                this.unitOfWork.SaveChanges();
                return new ReponseResult();
            }
            catch (Exception ex)
            {
                return new ReponseResult(ex.Message);
            }
        }
    }
}