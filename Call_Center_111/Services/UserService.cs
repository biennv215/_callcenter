﻿using Database.Model;
using Database.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services
{
    public class UserService : IUserService
    {
        private IUserRepository UserRepository { get; }
        public UserService(IUserRepository repository)
        {
            UserRepository = repository;
        }

        public User GetUser(string username, string password)
        {
            return UserRepository.GetUser(username, password);
            //throw new NotImplementedException();
        }

        public User ChangePassword(string username, string new_password)
        {
            return UserRepository.ChangePassword(username, new_password);
            //throw new NotImplementedException();
        }

    }
}
