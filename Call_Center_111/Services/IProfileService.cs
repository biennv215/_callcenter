﻿using Database.Model;
using System;
using System.Collections.Generic;
using System.Text;
using ViewModel;
using ViewModel.Profiles;

namespace Services
{
    public interface IProfileService
    {
        List<Profile> GetProfiles();
        Profile ProfileGetById(int profileid);
        List<Profile> GetProfileSearch(string phonenumber);
        bool UpdateProfile(int profileid, string phonenumber, string profilecontent, string profilestatus, int updateby);
        bool DeleteProfile(int profileid);

        IEnumerable<ProfileVM> GetProfiless();
        ProfileVM GetById(int id);

        ReponseResult Create(CreateProfileVM request);
        ReponseResult Update(UpdateProfileVM request);
        ReponseResult Delete(int id);

        IEnumerable<ProfileVM> PaginationProfiles(int start, int limit, string searchKey = "");
        int ProfileCount();
        int NumberPage(Func<Profile, bool> search, int limit);
    }
}
