﻿using Database.Infrastructures;
using Database.Model;
using Database.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel;
using ViewModel.Profiles;

namespace Services
{
    public class ProfileService : IProfileService
    {
        private IProfileRepository ProfileRepository { get; }
        private readonly IUnitOfWork unitOfWork = null;
        public ProfileService(IProfileRepository repository, IUnitOfWork unitOfWork)
        {
            ProfileRepository = repository;
            this.unitOfWork = unitOfWork;
        }

        public List<Profile> GetProfiles()
        {
            return ProfileRepository.GetProfile();
        }
        public Profile ProfileGetById(int profileid)
        {
            return ProfileRepository.ProfileGetById(profileid);
        }
        public List<Profile> GetProfileSearch(string phonenumber)
        {
            return ProfileRepository.GetProfileSearch(phonenumber);
        }
        public bool UpdateProfile(int profileid, string phonenumber, string profilecontent, string profilestatus, int updateby)
        {
            return ProfileRepository.UpdateProfile(profileid, phonenumber, profilecontent, profilestatus, updateby);
        }
        public bool DeleteProfile(int profileid)
        {
            return ProfileRepository.DeleteProfile(profileid);
        }



        public IEnumerable<ProfileVM> GetProfiless()
        {
            var profiles = this.unitOfWork.ProfileRepository.GetAll();
            foreach(var profile in profiles)
            {
                yield return new ProfileVM()
                {
                    ProfileId = profile.ProfileId,
                    PhoneNumber = profile.PhoneNumber,
                    ProblemContents = profile.ProblemContents,
                    Status = profile.Status,
                    CreateBy = profile.CreateBy,
                    CreateTime = profile.CreateTime,
                    UpdateBy = profile.UpdateBy,
                    UpdateTime = profile.UpdateTime,
                    ChildrenId = profile.ChildrenId,
                    ProvinceId = profile.ProvinceId,
                    DistrictId = profile.DistrictId,
                    WardsId = profile.WardsId,
                };
            }
        }

        public ProfileVM GetById(int id)
        {
            var profile = this.unitOfWork.ProfileRepository.GetById(id);
            if(profile != null && profile.IsDeleted == false)
            {
                return new ProfileVM()
                {
                    ProfileId = profile.ProfileId,
                    PhoneNumber = profile.PhoneNumber,
                    ProblemContents = profile.ProblemContents,
                    Status = profile.Status,
                    CreateBy = profile.CreateBy,
                    CreateTime = profile.CreateTime,
                    UpdateBy = profile.UpdateBy,
                    UpdateTime = profile.UpdateTime,
                    ChildrenId = profile.ChildrenId,
                    ProvinceId = profile.ProvinceId,
                    DistrictId = profile.DistrictId,
                    WardsId = profile.WardsId,
                };
            }
            return new ProfileVM();
        }

        public ReponseResult Create(CreateProfileVM request)
        {
            try
            {
                var profile = new Profile()
                {
                    PhoneNumber = request.PhoneNumber,
                    ProblemContents = request.ProblemContents,
                    Status = request.Status,
                    CreateBy = 1,
                    CreateTime = DateTime.Now,
                    ChildrenId = request.ChildrenId,
                    ProvinceId = request.ProvinceId,
                    DistrictId = request.DistrictId,
                    WardsId = request.WardsId,
                };
                this.unitOfWork.ProfileRepository.Add(profile);
                this.unitOfWork.SaveChanges();
                return new ReponseResult();
            }
            catch(Exception ex)
            {
                return new ReponseResult(ex.Message);
            }
        }

        public ReponseResult Update(UpdateProfileVM request)
        {
            try
            {
                var profile = this.unitOfWork.ProfileRepository.GetById(request.ProfileId);
                if(profile == null)
                {
                    return new ReponseResult($"Không tìm thấy đối tượng {request.ProfileId}");
                }
                profile.PhoneNumber = request.PhoneNumber;
                profile.ProblemContents = request.ProblemContents;
                profile.Status = request.Status;
                profile.UpdateBy = 1;
                profile.UpdateTime = DateTime.Now;
                profile.IsDeleted = request.IsDelete;
                profile.ChildrenId = request.ChildrenId;
                profile.ProvinceId = request.ProvinceId;
                profile.DistrictId = request.DistrictId;
                profile.WardsId = request.WardsId;
                this.unitOfWork.SaveChanges();
                return new ReponseResult();
            }
            catch(Exception ex)
            {
                return new ReponseResult(ex.Message);
            }
        }

        public ReponseResult Delete(int id)
        {
            try
            {
                var profile = this.unitOfWork.ProfileRepository.GetById(id);
                if (profile == null)
                {
                    return new ReponseResult($"Không tìm thấy đối tượng {id}");
                }
                profile.IsDeleted = true;
                this.unitOfWork.SaveChanges();
                return new ReponseResult();
            }
            catch (Exception ex)
            {
                return new ReponseResult(ex.Message);
            }
        }

        public IEnumerable<ProfileVM> PaginationProfiles(int start, int limit, string searchKey = "")
        {
            if (String.IsNullOrEmpty(searchKey))
                searchKey = "";
            var profiles = this.unitOfWork.ProfileRepository.Pagination(start, limit, x => x.ProfileId, x => x.PhoneNumber.Contains(searchKey));
            foreach(var profile in profiles)
            {
                yield return new ProfileVM()
                {
                    ProfileId = profile.ProfileId,
                    PhoneNumber = profile.PhoneNumber,
                    ProblemContents = profile.ProblemContents,
                    Status = profile.Status,
                    CreateBy = profile.CreateBy,
                    CreateTime = profile.CreateTime,
                    UpdateBy = profile.UpdateBy,
                    UpdateTime = profile.UpdateTime,
                    ChildrenId = profile.ChildrenId,
                    ProvinceId = profile.ProvinceId,
                    DistrictId = profile.DistrictId,
                    WardsId = profile.WardsId,
                };
            }
        }

        public int ProfileCount()
        {
            return this.unitOfWork.ProfileRepository.Count(x=>x.IsDeleted == false);
        }

        public int NumberPage(Func<Profile, bool> search, int limit)
        {
            return this.unitOfWork.ProfileRepository.NumberPage(search, limit);
        }
    }
}
