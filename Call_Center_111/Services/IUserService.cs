﻿using Database.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services
{
    public interface IUserService
    {
        User GetUser(string username, string password);
        User ChangePassword(string username, string new_password);
    }
}
