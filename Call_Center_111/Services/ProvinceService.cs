﻿using Database.Infrastructures;
using System.Collections.Generic;
using ViewModel.Provinces;

namespace Services
{
    public class ProvinceService : IProvinceService
    {
        private readonly IUnitOfWork unitOfWork = null;
        public ProvinceService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public IEnumerable<ProvinceVM> GetProvinces()
        {
            var Provinces = this.unitOfWork.ProvinceRepository.GetAll();
            foreach(var province in Provinces)
            {
                yield return new ProvinceVM()
                {
                    ProvinceId = province.ProvinceId,
                    ProvinceName = province.ProvinceName,
                };
            }
        }
    }
}