﻿using Database.Infrastructures;
using System.Collections.Generic;
using ViewModel.Districts;

namespace Services
{
    public class DistrictService : IDistrictService
    {
        private readonly IUnitOfWork unitOfWork = null;
        public DistrictService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public IEnumerable<DistrictVM> GetDistricts()
        {
            var districts = this.unitOfWork.DistrictRepository.GetAll();
            foreach(var district in districts)
            {
                yield return new DistrictVM()
                {
                    DistrictId = district.DistrictId,
                    DistrictName = district.DistrictName,
                    ProviceId = district.ProvinceId,
                };
            }
        }

        public IEnumerable<DistrictVM> GetDistrictsByProvinceId(int ProvinceId)
        {
            var districts = this.unitOfWork.DistrictRepository.Find(x => x.ProvinceId == ProvinceId);
            foreach (var district in districts)
            {
                yield return new DistrictVM()
                {
                    DistrictId = district.DistrictId,
                    DistrictName = district.DistrictName,
                    ProviceId = district.ProvinceId,
                };
            }
        }
    }
}