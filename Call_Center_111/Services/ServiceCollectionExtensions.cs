﻿using Microsoft.Extensions.DependencyInjection;

namespace Services
{
    public static class ServiceCollectionExtensions
    {
        public static void UseMyService(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IProfileService, ProfileService>();
            services.AddScoped<IChildrenService, ChildrenService>();
            services.AddScoped<IProvinceService, ProvinceService>();
            services.AddScoped<IDistrictService, DistrictService>();
            services.AddScoped<IWardsService, WardsService>();
        }
    }
}