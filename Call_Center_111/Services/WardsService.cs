﻿using Database.Infrastructures;
using System.Collections.Generic;
using ViewModel.Wards;

namespace Services
{
    public class WardsService : IWardsService
    {
        private readonly IUnitOfWork unitOfWork = null;
        public WardsService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public IEnumerable<WardsVM> GetWards()
        {
            var wards = this.unitOfWork.WardsRepository.GetAll();
            foreach(var ward in wards)
            {
                yield return new WardsVM()
                {
                    WardsId = ward.WardsId,
                    WardsName = ward.WardsName,
                    DistrictId = ward.DistrictId,
                };
            }
        }

        public IEnumerable<WardsVM> GetWardsByDistrictId(int DistrictID)
        {
            var wards = this.unitOfWork.WardsRepository.Find(x=>x.DistrictId == DistrictID);
            foreach (var ward in wards)
            {
                yield return new WardsVM()
                {
                    WardsId = ward.WardsId,
                    WardsName = ward.WardsName,
                    DistrictId = ward.DistrictId,
                };
            }
        }
    }
}