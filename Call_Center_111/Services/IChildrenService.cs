﻿using Database.Model;
using System;
using System.Collections.Generic;
using ViewModel;
using ViewModel.Childrens;

namespace Services
{
    public interface IChildrenService
    {
        IEnumerable<ChildrenVM> GetChildrens();
        ChildrenVM GetById(int id);
        ChildrenVM GetChildrenNew();

        ReponseResult Create(CreateChildrenVM request);
        ReponseResult Update(UpdateChildrenVM request);
        ReponseResult Delete(int id);

        IEnumerable<ChildrenVM> PaginationChildrens(int start, int limit, string searchKey = "");
        int ChildrenCount();
        int NumberPage(Func<Children, bool> search, int limit);
    }
}