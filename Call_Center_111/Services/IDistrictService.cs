﻿using System.Collections.Generic;
using ViewModel.Districts;

namespace Services
{
    public interface IDistrictService
    {
        IEnumerable<DistrictVM> GetDistricts();

        IEnumerable<DistrictVM> GetDistrictsByProvinceId(int ProvinceId);
    }
}