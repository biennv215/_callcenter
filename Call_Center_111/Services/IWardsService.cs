﻿using System.Collections.Generic;
using ViewModel.Wards;

namespace Services
{
    public interface IWardsService
    {
        IEnumerable<WardsVM> GetWards();
        IEnumerable<WardsVM> GetWardsByDistrictId(int DistrictId);
    }
}