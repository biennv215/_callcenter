﻿using System;

namespace ViewModel.Childrens
{
    public class UpdateChildrenVM
    {
        public int ChildrenId { get; set; }
        public string ChildrenName { get; set; }
        public int Age { get; set; }
        public bool Gender { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
    }
}