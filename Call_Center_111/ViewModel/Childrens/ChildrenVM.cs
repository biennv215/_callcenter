﻿using System;

namespace ViewModel.Childrens
{
    public class ChildrenVM
    {
        public int ChildrenId { get; set; }
        public string ChildrenName { get; set; }
        public int Age { get; set; }
        public bool Gender { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateTime { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public bool IsDelete { get; set; }
    }
}