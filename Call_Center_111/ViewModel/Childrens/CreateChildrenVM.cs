﻿namespace ViewModel.Childrens
{
    public class CreateChildrenVM
    {
        public string ChildrenName { get; set; }
        public int Age { get; set; }
        public bool Gender { get; set; }
        public int CreateBy { get; set; }
    }
}