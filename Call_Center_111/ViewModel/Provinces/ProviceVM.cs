﻿namespace ViewModel.Provinces
{
    public class ProvinceVM
    {
        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }
    }
}