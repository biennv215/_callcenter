﻿namespace ViewModel
{
    public class ReponseResult
    {
        public bool Issuccessed { get; set; }
        public string ErrorMessage { get; set; }

        public ReponseResult()
        {
            this.Issuccessed = true;
        }

        public ReponseResult(string message)
        {
            this.Issuccessed = false;
            this.ErrorMessage = message;
        }
    }
}