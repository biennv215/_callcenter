﻿namespace ViewModel.Districts
{
    public class DistrictVM
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }

        public int ProviceId { get; set; }
    }
}