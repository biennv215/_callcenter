﻿namespace ViewModel.Wards
{
    public class WardsVM
    {
        public int WardsId { get; set; }
        public string WardsName { get; set; }

        public int DistrictId { get; set; }
    }
}