﻿using System;

namespace ViewModel.Profiles
{
    public class CreateProfileVM
    {
        public string PhoneNumber { get; set; }
        public string ProblemContents { get; set; }
        public string Status { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateTime { get; set; }

        public int ChildrenId { get; set; }

        public int ProvinceId { get; set; }

        public int DistrictId { get; set; }

        public int WardsId { get; set; }
    }
}