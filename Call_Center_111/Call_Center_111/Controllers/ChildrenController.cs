﻿using Microsoft.AspNetCore.Mvc;
using Services;

namespace Call_Center_111.Controllers
{
    public class ChildrenController : Controller
    {
        private readonly IChildrenService childrenService = null;

        public ChildrenController(IChildrenService childrenService)
        {
            this.childrenService = childrenService;
        }

        public IActionResult Index(int? page = 1, int limit = 10, string searchKey = "")
        {
            ViewBag.pageCurrent = page;
            ViewBag.totalChildren = this.childrenService.ChildrenCount();
            ViewData["Limit"] = limit;
            if (searchKey == null)
                searchKey = "";
            ViewBag.numberPage = this.childrenService.NumberPage(x=>x.IsDeleted==false && x.ChildrenName.Contains(searchKey), limit);
            ViewData["SearchKey"] = searchKey;
            int start;
            if (page <= 0)
            {
                page = 1;
            }
            start = (int)(page - 1) * limit;
            var childrens = childrenService.PaginationChildrens(start, limit, searchKey);
            return View(childrens);
        }


    }
}