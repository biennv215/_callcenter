﻿using Microsoft.AspNetCore.Mvc;
using Services;

namespace Call_Center_111.Controllers
{
    public class ProfileController : Controller
    {
        private readonly IProfileService profileService;
        public ProfileController(IProfileService profileService)
        {
            this.profileService = profileService;
        }
        public IActionResult Index(int? page = 1, int limit = 10, string searchKey = "")
        {
            ViewBag.pageCurrent = page;
            ViewBag.totalProfile = this.profileService.ProfileCount();
            ViewData["Limit"] = limit;
            if (searchKey == null)
                searchKey = "";
            ViewBag.numberPage = this.profileService.NumberPage(x => x.PhoneNumber.Contains(searchKey), limit);
            ViewData["SearchKey"] = searchKey;
            int start;
            if (page <= 0)
                page = 1;
            start = (int)(page - 1) * limit;
            var profiles = this.profileService.PaginationProfiles(start, limit, searchKey);
            return View(profiles);
        }
        public IActionResult UpdateProfile(int id)
        {
            var result = profileService.ProfileGetById(id);   
            return View(result);
        }
        [HttpPost]
        public IActionResult UpdateProfile(int profileid, string phone, string content, string status, int updateby)
        {
            var result = profileService.UpdateProfile(profileid, phone, content, status, updateby);
            return View();
        }
        public IActionResult DeleteProfile(int id)
        {
            var result = profileService.DeleteProfile(id);
            return RedirectToAction("Index");
        }
    }
}
