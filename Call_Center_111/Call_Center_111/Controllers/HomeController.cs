﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient.Server;
using Services;

namespace Call_Center_111.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserService _userService;
        public HomeController(ILogger<HomeController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }
        public IActionResult Login()
        {
            return View();
        }
        //[Route("login")]
        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            if (username != null && password != null && _userService.GetUser(username, password) != null)
            {
                HttpContext.Session.SetString("username", username);
                return RedirectToAction("Index","Profile");
            }
            else
            {
                ViewBag.error = "Invalid Account";
                return View("Login");
            }
        }
        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ChangePassword(string username, string password, string newpass, string newpassConf)
        {
            if(username == null || password == null || newpassConf == null || newpass == null)
            {
                ViewBag.error = "Vui lòng nhập đầy đủ các trường!";
                return View();
            } else if(newpass != newpassConf)
            {
                ViewBag.error = "Vui lòng nhập lại mật khẩu đúng với mật khẩu mới!";
                return View();
            }
            else if(_userService.GetUser(username, password) == null)
            {
                ViewBag.error = "Vui lòng nhập lại mật khẩu đúng !";
                return View();
            }
            else
            {
                _userService.ChangePassword(username, newpass);
                return RedirectToAction("Index","Profile");
            }
        }
        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ForgotPassword(string username, string password)
        {
            return View();
        }
        [HttpGet]
        public IActionResult ResetPassword()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ResetPassword(string username, string password)
        {
            return View();
        }
        
    }
}
