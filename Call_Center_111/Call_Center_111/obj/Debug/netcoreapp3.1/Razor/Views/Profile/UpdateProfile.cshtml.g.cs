#pragma checksum "C:\Users\Bien Nguyen\Documents\callcenter\Call_Center_111\Call_Center_111\Views\Profile\UpdateProfile.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f4c3e5673d9cee24e71a910a8aac6d3371182906"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Profile_UpdateProfile), @"mvc.1.0.view", @"/Views/Profile/UpdateProfile.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f4c3e5673d9cee24e71a910a8aac6d3371182906", @"/Views/Profile/UpdateProfile.cshtml")]
    public class Views_Profile_UpdateProfile : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Database.Model.Profile>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\Bien Nguyen\Documents\callcenter\Call_Center_111\Call_Center_111\Views\Profile\UpdateProfile.cshtml"
  
    ViewData["Title"] = "UpdateProfile";
    Layout = "/Views/Shared/_Default.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h2>Chỉnh sửa hồ sơ</h2>\r\n\r\n<div class=\"row\">\r\n    <div class=\"tt col-4\">\r\n        <div>\r\n            <label for=\"selt1\" id=\"option\">Số điện thoại:</label>\r\n            <input class=\"form-control\" type=\"text\"");
            BeginWriteAttribute("value", " value=\"", 339, "\"", 365, 1);
#nullable restore
#line 14 "C:\Users\Bien Nguyen\Documents\callcenter\Call_Center_111\Call_Center_111\Views\Profile\UpdateProfile.cshtml"
WriteAttributeValue("", 347, Model.PhoneNumber, 347, 18, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" id=\"phone\">\r\n        </div>\r\n        <div>\r\n            <label for=\"selt1\" id=\"option\">Nội dung vấn đề:</label>\r\n            <input class=\"form-control\" type=\"text\"");
            BeginWriteAttribute("value", " value=\"", 531, "\"", 561, 1);
#nullable restore
#line 18 "C:\Users\Bien Nguyen\Documents\callcenter\Call_Center_111\Call_Center_111\Views\Profile\UpdateProfile.cshtml"
WriteAttributeValue("", 539, Model.ProblemContents, 539, 22, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" id=\"content\">\r\n        </div>\r\n        <div>\r\n            <label for=\"selt1\" id=\"status\">Trạng thái:</label>\r\n            <select id=\"Status\"");
            BeginWriteAttribute("name", " name=\"", 704, "\"", 724, 1);
#nullable restore
#line 22 "C:\Users\Bien Nguyen\Documents\callcenter\Call_Center_111\Call_Center_111\Views\Profile\UpdateProfile.cshtml"
WriteAttributeValue("", 711, Model.Status, 711, 13, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" class=""form-control"">
                <option value=""Đang mở"">Đang mở</option>
                <option value=""Đang xử lý"">Đang xử lý</option>
                <option value=""Đã xử lý"">Đã xử lý</option>
            </select>
        </div>
    </div>
    <div class=""tt col-4"">
        <div class=""form-group"">
            <label for=""selt1"" id=""province"">Tỉnh - Thành phố:</label>
            <select class=""form-control"">
                <option>Hà Nội</option>
                <option>Hà Giang</option>
                <option>Bắc Ninh </option>
                <option>Hải Phòng</option>
            </select>
        </div>
        <div>
            <label for=""selt1"" id=""district"">Quận - Huyện:</label>
            <select class=""form-control"">
                <option>Thạch Thất</option>
                <option>Cầu Giấy</option>
                <option>Nam Từ Liêm</option>
                <option>Đống Đa</option>
            </select>
        </div>
        <div>
            <label for=");
            WriteLiteral(@"""selt1"" id=""ward"">Xã - Phường - Thị trấn:</label>
            <select class=""form-control"">
                <option>Quan Hoa</option>
                <option>Trung Hòa</option>
                <option>Phú Đô</option>
                <option>Đội Cấn</option>
            </select>
        </div>
    </div>
    <div class=""tt col-4"">
        <div>
            <label for=""selt1"" id=""name"">Họ tên trẻ:</label>
            <input class=""form-control"" type=""text"" name=""ChildId"" id=""name"">
        </div>
        <div>
            <label for=""selt1"" id=""age"">Tuổi:</label>
            <input class=""form-control"" type=""number"" name=""age"" id=""age"">
        </div>
        <div>
            <label for=""selt1"" id=""gender"">Giới tính:</label>
            <select class=""form-control"">
                <option>Nam</option>
                <option>Nữ</option>
            </select>
        </div>
    </div>
    <div class=""d-flex justify-content-between align-items-between"">
        <a href=""#"" class=""btn");
            WriteLiteral(" btn-success nav-link text-white\">Lưu thay đổi</a>\r\n        <a");
            BeginWriteAttribute("href", " href=\"", 2835, "\"", 2872, 1);
#nullable restore
#line 77 "C:\Users\Bien Nguyen\Documents\callcenter\Call_Center_111\Call_Center_111\Views\Profile\UpdateProfile.cshtml"
WriteAttributeValue("", 2842, Url.Action("Index","Profile"), 2842, 30, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" class=""btn btn-danger nav-link text-white"">Hủy</a>
    </div>
</div>

<script>
    $(document).ready(function(){
        var status = $(""#Status"").attr(""name"");
        $(""#Status [value='""+status+""']"").attr(""selected"",""selected"");
        console.log(status);
        console.log(""#Status [value='""+status+""']"");
    });
</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Database.Model.Profile> Html { get; private set; }
    }
}
#pragma warning restore 1591
