﻿using System;

namespace Call_Center_111.Code
{
    [Serializable]
    public class UserSession
    {
        public string UserName { set; get; }
    }
}
