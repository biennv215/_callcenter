﻿using Microsoft.AspNetCore.Mvc;
using Services;
using ViewModel.Profiles;

namespace Call_Center_111.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileService profileService = null;

        public ProfileController(IProfileService profileService)
        {
            this.profileService = profileService;
        }

        [HttpGet]
        [Route("GetProfileById")]
        public ActionResult GetProfileById(int ProfileId)
        {
            return Ok(profileService.GetById(ProfileId));
        }

        [HttpPost]
        [Route("AddProfile")]
        public ActionResult AddProfile(CreateProfileVM request)
        {
            profileService.Create(request);
            return Ok(profileService.GetProfiless());
        }
    }
}