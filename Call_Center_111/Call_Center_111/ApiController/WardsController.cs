﻿using Microsoft.AspNetCore.Mvc;
using Services;

namespace Call_Center_111.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class WardsController : ControllerBase
    {
        private readonly IWardsService wardsService = null;
        public WardsController(IWardsService wardsService)
        {
            this.wardsService = wardsService;
        }

        [HttpGet]
        [Route("GetWards")]
        public ActionResult GetWards()
        {
            return Ok(wardsService.GetWards());
        }

        [HttpGet]
        [Route("GetWardsByDistrictId")]
        public ActionResult GetWardsByDistrictId(int DistrictId)
        {
            return Ok(wardsService.GetWardsByDistrictId(DistrictId));
        }
    }
}