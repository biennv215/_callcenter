﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewModel.Childrens;

namespace Call_Center_111.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChildrenController : ControllerBase
    {
        private readonly IChildrenService childrenService = null;

        public ChildrenController(IChildrenService childrenService)
        {
            this.childrenService = childrenService;
        }

        [HttpGet]
        [Route("GetChildrens")]
        public ActionResult GetChildrens(int? page = 0, int limit = 5, string searchKey = "")
        {

            //int limit = 5;
            int start;
            if (page <= 0)
            {
                page = 1;
            }
            start = (int)(page - 1) * limit;
            var childrens = childrenService.PaginationChildrens(start, limit, searchKey);
            return Ok(childrens);
        }

        [HttpGet]
        [Route("GetChildrenById")]
        public ActionResult GetChildrenById(int id)
        {
            return Ok(childrenService.GetById(id));
        }

        [HttpGet]
        [Route("GetChildrenNew")]
        public ActionResult GetChildrenNew()
        {
            return Ok(childrenService.GetChildrenNew());
        }

        [HttpPost]
        [Route("AddChildren")]
        public ActionResult AddChildren(CreateChildrenVM request)
        {
            childrenService.Create(request);
            return Ok(childrenService.GetChildrens());
        }

        [HttpPut]
        [Route("UpdateChildren")]
        public ActionResult UpdateChildren(UpdateChildrenVM request)
        {
            childrenService.Update(request);
            return Ok(childrenService.GetChildrens());
        }

        [HttpDelete]
        [Route("DeleteChildren")]
        public ActionResult DeleteChildren(int id)
        {
            childrenService.Delete(id);
            return Ok(childrenService.GetChildrens());
        }
    }
}
