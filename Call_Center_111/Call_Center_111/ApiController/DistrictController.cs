﻿using Microsoft.AspNetCore.Mvc;
using Services;

namespace Call_Center_111.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class DistrictController : ControllerBase
    {
        private readonly IDistrictService districtServicel = null;
        public DistrictController(IDistrictService districtService)
        {
            this.districtServicel = districtService;
        }

        [HttpGet]
        [Route("GetDistricts")]
        public ActionResult GetDistricts()
        {
            return Ok(districtServicel.GetDistricts());
        }

        [HttpGet]
        [Route("GetDistrictsByProvinceId")]
        public ActionResult GetDistrictsByProvinceId(int ProvinceId)
        {
            return Ok(districtServicel.GetDistrictsByProvinceId(ProvinceId));
        }
    }
}