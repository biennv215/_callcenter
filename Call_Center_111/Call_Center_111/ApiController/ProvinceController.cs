﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Call_Center_111.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProvinceController : ControllerBase
    {
        private readonly IProvinceService provinceService = null;
        public ProvinceController(IProvinceService provinceService)
        {
            this.provinceService = provinceService;
        }
        
        [HttpGet]
        [Route("GetProvinces")]
        public ActionResult GetProvinces()
        {
            return Ok(provinceService.GetProvinces());
        }
    }
}
